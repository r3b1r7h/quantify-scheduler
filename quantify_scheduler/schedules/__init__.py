# Repository: https://gitlab.com/quantify-os/quantify-scheduler
# Licensed according to the LICENCE file on the main branch
"""
Module containing a standard library of schedules for common experiments as well as the
:class:`.ScheduleBase`, :class:`.Schedule`, and :class:`.CompiledSchedule` classes.


.. tip::

    The source code of the schedule generating functions in this module can
    serve as examples when creating schedules for custom experiments.

"""
